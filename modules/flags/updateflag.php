<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

var_dump('update flag');

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'UpdateFlag' )   and
     $Module->hasActionParameter( 'FlagID' ) and
     $Module->hasActionParameter( 'FlagName' ) and
     $Module->hasActionParameter( 'Category' ) )
{
    $flag_id = (int) $Module->actionParameter( 'FlagID' );
    $flag_name = $Module->actionParameter( 'FlagName' );
    $flag_priority = $Module->actionParameter( 'FlagPriority' );
    $category = $Module->actionParameter( 'Category' );
}

if ( $flag_name )
{
    $flag = eZPersistentObject::fetchObject( easycmsFlagObject::definition(), null, array('id' => $flag_id) );
    $flag->setAttribute( 'name', $flag_name );
    $flag->setAttribute( 'priority', $flag_priority );
    $flag->store();
    $flag_category = eZPersistentObject::fetchObject( easycmsFlagCategoryLinkObject::definition(), null,  array('flag_id'=> $flag_id ) );
    if ( $flag_category instanceof easycmsFlagCategoryLinkObject )
        $flag_category->remove();
    if ($category != "empty"){
        $new_cat = new easycmsFlagCategoryLinkObject( array('flag_id'=>$flag_id, 'flag_category_id' => (int) $category ) );
        $new_cat->store();
    }
}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( $Module->actionParameter( 'RedirectRelativeURI' ) ) : $Module->redirectTo( '/' );

?>


