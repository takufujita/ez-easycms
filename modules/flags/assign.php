<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';

// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'Assign' )   and
     $Module->hasActionParameter( 'ObjectID' )   and
     $Module->hasActionParameter( 'FlagList' ) )
{
    $flags = $Module->actionParameter( 'Flags' );
    $flaglist = $Module->actionParameter( 'FlagList' );
    $flaglist = explode(',', substr($flaglist,1));
    $objectID = $Module->actionParameter( 'ObjectID' );
}


// Change object's state
if ( $objectID and $flaglist )
{
    foreach($flaglist as $flag_id){
        $objectID = (int) $objectID;
        $flag_id = (int) $flag_id;
        $checked = $flags[$flag_id] === "on";
        $flag_link = easycmsFlagLinkObject::exists( $objectID, $flag_id );
        if ( ($checked) and (! ($flag_link instanceof easycmsFlagLinkObject))){
            $link = new easycmsFlagLinkObject( array('contentobject_id'=>$objectID, 'flag_id'=>$flag_id) );
            $link->store();
        }elseif ( (! $checked) and ($flag_link instanceof easycmsFlagLinkObject) ){
            $flag_link->remove();
        }
    }
}

$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( rawurlencode( $Module->actionParameter( 'RedirectRelativeURI' ) ) ) : $Module->redirectTo( '/' );

?>
