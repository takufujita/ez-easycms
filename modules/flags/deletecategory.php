<?php

$Module = $Params['Module'];
$Result = array();
$Result['content'] = '';


// Identify whether the input data was submitted through URL parameters or through POST
if ( $Module->isCurrentAction( 'DeleteCategory' )   and
     $Module->hasActionParameter( 'CategoryID' ) )
{
    $category_id = (int) $Module->actionParameter( 'CategoryID' );
}

if ( $category_id )
{
    $category = eZPersistentObject::fetchObject( easycmsFlagCategoryObject::definition(), null, array('id' => $category_id) );
    $category->remove();
    $category_links = eZPersistentObject::fetchObjectList( easycmsFlagCategoryLinkObject::definition(), null, array('flag_category_id' => $category_id) );
    foreach( $category_links as $category_link ){
        $category_link->remove();
    }

}
$Module->hasActionParameter( 'RedirectRelativeURI' ) ? $Module->redirectTo( $Module->actionParameter( 'RedirectRelativeURI' ) ) : $Module->redirectTo( '/' );

?>

