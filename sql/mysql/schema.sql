CREATE TABLE `easycms_flags` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `contentobject_id` int(11) NOT NULL,
  `priority` int(11),
  PRIMARY KEY ( `id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_flags_category` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `contentobject_id` int(11) NOT NULL,
  `system_category` BOOL,
  PRIMARY KEY ( `id` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_flags_link` (
  `contentobject_id` int(11) NOT NULL,
  `flag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_flags_category_link` (
  `flag_id` int(11) NOT NULL,
  `flag_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_template_link` (
  `node_id` int(11) NOT NULL,
  `view` varchar(255) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_childtemplate_link` (
  `node_id` int(11) NOT NULL,
  `view` varchar(255) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `easycms_layout_link` (
  `node_id` int(11) NOT NULL,
  `layout` varchar(255) NOT NULL default ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
