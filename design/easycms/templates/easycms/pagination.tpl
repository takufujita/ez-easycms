{if gt($max_pages,1)}
<div class="pagination">
{if gt($page_nb,1)}
<span class="prev"><a rel="next" href={concat($base_url, '/(offset)/', mul($page_nb|dec|dec,$items_per_page),'/',$extra_parameters)|ezurl}>&lt;&lt;</a></span>
{/if}

{if and(gt($max_pages,15), gt($page_nb,6))}
{def $end_page = min(sum($page_nb,6),$max_pages)}
{def $p_offset = sub(6,min(sub($max_pages,$page_nb),6))}
{def $start_page = sub($page_nb,6,$p_offset)}
{else}
{def $start_page = 1}
{def $end_page = min(13,$max_pages)}
{/if}

{for $start_page to $end_page as $index} 
{if eq($index, $page_nb)}
<span class="current">{$index}</span>
{else}
<span><a href={concat($base_url, '/(offset)/', mul($index|dec,$items_per_page),'/',$extra_parameters)|ezurl}>{$index}</a></span>
{/if}
{/for}
{if lt($page_nb,$max_pages)}
<span class="next"><a rel="next" href={concat($base_url, '/(offset)/', mul($page_nb,$items_per_page),'/',$extra_parameters)|ezurl}>&gt;&gt;</a></span>
{/if}
</div>
{/if}
