{def $flag_data = fetch('flags','container_flag_data', hash(
    'node_id', $node.node_id
))}
<div class="container">

{foreach $flag_data as $category}
<div class="block">
<table class="list" cellspacing="0">
<tr>
<th width="100%">{$category.name}　{if ne($category.id, 'empty')}<span class="category-id">[id: {$category.id}]</span>{/if} {if $category.system_category}<span class="system-flag">システム</span>{/if}</th>
<th>ID</th>
<th>カテゴリー</th>
<th>優先度</th>
<th>
{if ne($category.id,'empty')}
<input type="submit" value="削除" class="button delete-category-button" />
<input type="hidden" name="category_id" value="{$category.id}" />
<input type="hidden" name="category_name" value="{$category.name}" />
{/if}
</th>
<th>
{if ne($category.id,'empty')}
<input type="submit" value="編集" class="button update-category-button" />
<input type="hidden" name="category_id" value="{$category.id}" />
<input type="hidden" name="category_name" value="{$category.name}" />
<input type="hidden" name="category_system" value="{$category.system_category}" />
{/if}
</th>
</tr>
{if $category.flags|count}
{foreach $category.flags as $flag sequence array('bg-light','bg-dark') as $style}
<tr class="{$style}">
<td>{$flag.name}</td>
<td style="text-align:center;">{$flag.id}</td>
<td style="text-align:center;">{$category.name}</td>
<td style="text-align:center;">{$flag.priority}</td>
<td>
<input type="submit" value="削除" class="button delete-flag-button hide" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
</td>
<td>
<input type="submit" value="編集" class="button update-flag-button hide" />
<input type="hidden" name="category" value="{$category.id}" />
<input type="hidden" name="flag_name" value="{$flag.name}" />
<input type="hidden" name="flag_priority" value="{$flag.priority}" />
<input type="hidden" name="flag_id" value="{$flag.id}" />
</td>
</tr>
{/foreach}
{else}
<tr class="bg-light">
<td colspan="3">フラグはありません</td>
</tr>
{/if}
</table>
<hr />
</div>
{/foreach}

<div>
<input type="submit" value="{'カテゴリの追加'|i18n( 'design/admin/node/view/full' )}" id="add-category-button" class="button hide" />
<input type="submit" value="フラグの追加" class="button hide" id="add-flag-button" title="新規フラグを追加する" />
</div>

{include uri='design:flags/add_flag_button.tpl'}
{include uri='design:flags/add_category_button.tpl'}
{include uri='design:flags/update_flag_button.tpl'}
{include uri='design:flags/update_category_button.tpl'}
{include uri='design:flags/delete_flag_button.tpl'}
{include uri='design:flags/delete_category_button.tpl'}
</div><!-- container -->
