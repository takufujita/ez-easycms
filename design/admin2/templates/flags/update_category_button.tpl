
    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="edit-category-modal-window" class="modal-window" style="display:none;">
        <h2>カテゴリの編集<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/updatecategory'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
        <input type="hidden" value="" name="CategoryID">
        <fieldset>
        <p>カテゴリ:<input type="text" name="CategoryName"></p>
        <p>システムカテゴリ:<input type="checkbox" name="CategorySystem"></p>
        <p><input type="submit" value="カテゴリの更新" class="button" name="UpdateCategoryButton"></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#edit-category-modal-window',
                centered: true,
                content: '.blank-content',
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                Y.all(".update-category-button").each(function(){
                    this.on('click', function (e) {
                        $category = this.siblings('[name=category_name]').get('value');
                        $category_id = this.siblings('[name=category_id]').get('value');
                        $category_system = this.siblings('[name=category_system]').get('value');
                        Y.one('#edit-category-modal-window [name=CategoryName]').setAttribute('value', $category);
                        Y.one('#edit-category-modal-window [name=CategoryID]').setAttribute('value', $category_id);
                        if ( $category_system == 1 ){
                            Y.one('#edit-category-modal-window [name=CategorySystem]').setAttribute('checked', 'checked');
                            }
                        win.open();
                        e.halt();
                        });

                    this.removeClass('hide');
                });
            });
        });

    })();
    {/literal}
    </script>
    {/run-once}



