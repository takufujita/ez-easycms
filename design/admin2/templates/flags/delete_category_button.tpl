
    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="delete-category-modal-window" class="modal-window" style="display:none;">
        <h2>フラグの削除<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/deletecategory'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
        <input type="hidden" value="" name="CategoryID">
        <fieldset>
        <p>カテゴリ: <span class="category-name"></span></p>
        <p><input type="submit" value="カテゴリの削除" class="button" name="DeleteCategoryButton"></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#delete-category-modal-window',
                centered: true,
                content: '.blank-content',
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                Y.all(".delete-category-button").each(function(){

                    this.on('click', function (e) {
                        $category_name = this.siblings('[name=category_name]').get('value');
                        $category_id = this.siblings('[name=category_id]').get('value');
                        Y.one('#delete-category-modal-window span.category-name').setContent(''+$category_name);
                        Y.one('#delete-category-modal-window [name=CategoryID]').setAttribute('value', $category_id);
                        win.open();
                        e.halt();
                        });

                    this.removeClass('hide');
                });
            });
        });

    })();
    {/literal}
    </script>
    {/run-once}

