{literal}
<script type="text/javascript">
(function () {
 YUI(YUI3_config).use('node', 'event', function (Y) {

   Y.on('domready', function() {
     Y.all(".toggle-button").each( function(button) {
       button.on('click', function (e) {
        this.ancestor('table').one('tbody').toggleView();
        this.getAttribute('value') == "表示" ? this.setAttribute('value', "非表示") : this.setAttribute('value', "表示");
        e.halt(); 
         });
       });
     });
   });

 })();
</script>
{/literal}

