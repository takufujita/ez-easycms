{def $categories = fetch('flags','categories', hash(
    'node_id', $node.node_id
))}

    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="edit-flag-modal-window" class="modal-window" style="display:none;">
        <h2>フラグの編集<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/updateflag'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
        <input type="hidden" value="" name="FlagID">
        <fieldset>
        <p>フラグ:<input type="text" name="FlagName"></p>
        <p>フラグの優先度:<input type="text" name="FlagPriority"></p>
        <p>フラグのカテゴリ:<select name="Category">
        <option value="empty">カテゴリなし</option>
{foreach $categories as $category}
        <option value="{$category.id}">{$category.name}</option>
{/foreach}
        </select></p>
        <p><input type="submit" value="フラグの更新" class="button" name="UpdateFlagButton"></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#edit-flag-modal-window',
                centered: true,
                content: '.blank-content',
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                Y.all(".update-flag-button").each(function(){

                    this.on('click', function (e) {
                        $category = this.siblings('[name=category]').get('value');
                        $flag_name = this.siblings('[name=flag_name]').get('value');
                        $flag_id = this.siblings('[name=flag_id]').get('value');
                        $flag_priority = this.siblings('[name=flag_priority]').get('value');
                        Y.one('#edit-flag-modal-window [name=FlagName]').setAttribute('value', $flag_name);
                        Y.one('#edit-flag-modal-window [name=FlagID]').setAttribute('value', $flag_id);
                        Y.one('#edit-flag-modal-window [name=FlagPriority]').setAttribute('value', $flag_priority);
                        Y.all('#edit-flag-modal-window select option').set('selected', '');
                        Y.one('#edit-flag-modal-window select [value="'+$category+'"]').set('selected', 'selected');
                        win.open();
                        e.halt();
                        });

                    this.removeClass('hide');
                });
            });
        });

    })();
    {/literal}
    </script>
    {/run-once}



