
    {run-once}
    {ezscript_require( array( 'ezjsc::yui3', 'ezjsc::yui3io', 'ezmodalwindow.js', 'ezajaxuploader.js' ) )}
    <div id="delete-flag-modal-window" class="modal-window" style="display:none;">
        <h2>フラグの削除<a href="#" class="window-close">{'Close'|i18n( 'design/admin/pagelayout' )}</a><span></span></h2>
        <div class="blank-content"></div>
        <div>
        <form action={'flags/deleteflag'|ezurl} method="post">
        <input type="hidden" value="{$node.url_alias}" name="RedirectRelativeURI">
        <input type="hidden" value="" name="FlagID">
        <fieldset>
        <p>フラグ: <span class="flag-name"></span></p>
        <p><input type="submit" value="フラグの削除" class="button" name="DeleteFlagButton"></p>
        </fieldset>
        </form>
        </div>
    </div>
    <script type="text/javascript">
    {literal}
    (function () {
        YUI(YUI3_config).use('ezmodalwindow', function (Y) {

            var windowConf = {
                window: '#delete-flag-modal-window',
                centered: true,
                content: '.blank-content',
                width: 400
            };

            Y.on('domready', function() {
                var win = new Y.eZ.ModalWindow(windowConf, Y);
                Y.all(".delete-flag-button").each(function(){

                    this.on('click', function (e) {
                        $flag_name = this.siblings('[name=flag_name]').get('value');
                        $flag_id = this.siblings('[name=flag_id]').get('value');
                        Y.one('#delete-flag-modal-window span.flag-name').setContent(''+$flag_name);
                        console.debug($flag_name);
                        Y.one('#delete-flag-modal-window [name=FlagID]').setAttribute('value', $flag_id);
                        win.open();
                        e.halt();
                        });

                    this.removeClass('hide');
                });
            });
        });

    })();
    {/literal}
    </script>
    {/run-once}



