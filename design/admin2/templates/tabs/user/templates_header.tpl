    {* Policies *}
    <li id="node-tab-templates" class="{if $last}last{else}middle{/if}{if $node_tab_index|eq('flags')} selected{/if}">
        {if $tabs_disabled}
            <span class="disabled" title="{'Tab is disabled, enable with toggler to the left of these tabs.'|i18n( 'design/admin/node/view/full' )}">
            {'テンプレート'|i18n( 'design/admin/node/view/full' )}</span>
        {else}
            <a href={concat( $node_url_alias, '/(tab)/flags' )|ezurl} title="{'flag setting interface.'|i18n( 'design/admin/node/view/full' )}">
            {'テンプレート'|i18n( 'design/admin/node/view/full' )}</a>
        {/if}
    </li>
