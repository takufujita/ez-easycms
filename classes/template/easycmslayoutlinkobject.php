<?php

class easycmsLayoutLinkObject extends eZPersistentObject
{
    /**
     * Constructor
     *
     */
    function __construct( $row )
    {
        parent::__construct( $row );
    }


    static function definition()
    {
        return array( 'fields'              => array( 'node_id'                   => array( 'name'     => 'node_id',
                                                                                            'datatype' => 'integer',
                                                                                            'default'  => 0,
                                                                                            'required' => true ),
                                                      'layout'                      => array( 'name'     => 'layout',
                                                                                            'datatype' => 'string',
                                                                                            'default'  => '',
                                                                                            'required' => false ) ),
                      'keys'                => array( 'node_id' ),
                      'class_name'          => 'easycmsLayoutLinkObject',
                      'sort'                => array( 'priority' => 'asc' ),
                      'name'                => 'easycms_layout_link' );
    }

    static function implicit_layout( $node_id )
    {
      if( $link = eZPersistentObject::fetchObject( self::definition(), null, array( 'node_id' => $node_id ) ) )
      {
        return $link->attribute( 'layout' );
      } else {
        return false;
      }
    }

    static function layout( $node_id )
    {
      $layouts_list_settings = eZINI::instance('easycms.ini')->variable('Templates', 'layouts');
      $base_node = eZContentObjectTreeNode::fetch($node_id);
      $node = $base_node;
      $implicit_layout = easycmsLayoutLinkObject::implicit_layout( $node_id );
      $layout = $implicit_layout;
      $herited_type = $implicit_viewmode == 'inherit' ? true : false;
      if( $implicit_viewmode == false || $implicit_viewmode == 'inherit' ){
        while($node->attribute('node_id') != "1")
        {
          $current_layout = easycmsLayoutLinkObject::implicit_layout( $node->attribute('node_id') );
          if( $current_layout != false and $current_layout != "inherit" )
          {
            $herited_type = 'node';
            $layout = $current_layout;
            break;
          }
          $node = $node->attribute('parent');
        }
      }
      if($node->attribute('node_id') == "1"){
        $layout = 'default';
      }
      return array( 
          'layout' => $layout, 
          'layout_name' => $layouts_list_settings[$layout], 
          'implicit_layout' => $implicit_layout, 
          'herited' => ($base_node != $node),
          'herited_from' => $node,
      );
    }

}

?>
